# -*- coding: utf-8 -*-
"""
/***************************************************************************
 AccAssess
                                 A QGIS plugin
 Raster classification accuracy assessment tools
                             -------------------
        begin                : 2013-01-20
        copyright            : (C) 2013 by Jared Kibele
        email                : jkibele@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


def name():
    return "Accuracy Assessment"


def description():
    return "Raster classification accuracy assessment tools"


def version():
    return "Version 0.3"


def icon():
    return "icon.png"


def qgisMinimumVersion():
    return "1.7"

def author():
    return "Jared Kibele"

def email():
    return "jkibele@gmail.com"

def classFactory(iface):
    # load AccAssess class from file AccAssess
    from accassess import AccAssess
    return AccAssess(iface)

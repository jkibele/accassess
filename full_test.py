# -*- coding: utf-8 -*-
from raster_handling import *
from error_matrix import *
import tempfile, os


def nothing():
    """
    This is just here so I can make a doc test that creates an error matrix 
    from two rasters and prints a csv.
    
    >>> rds = RasterDS( None, filepath='test_data/AccAssess_training.gis' )
    >>> cds = RasterDS( None, filepath='test_data/AccAssess_mlc.tif' )
    >>> em = cds.compare_to_reference( rds )
    >>> nt = tempfile.NamedTemporaryFile( mode="w+b", delete=False )
    >>> ntfn = nt.name
    >>> em.save_csv( nt )
    >>> nt.close()
    >>> nt2 = open( ntfn )
    >>> reader = csv.reader( nt2 )
    >>> for row in reader: print ', '.join( row )
    , 1, 2, 3, 4, Totals, Accuracy
    1, 618, 0, 0, 3, 621, 100
    2, 0, 570, 2, 0, 572, 100
    3, 0, 0, 313, 2, 315, 99
    4, 0, 0, 1, 383, 384, 100
    Totals, 618, 570, 316, 388, 1892, 
    Accuracy, 100, 100, 99, 99, , 100
    <BLANKLINE>
    Quantity Disagreement, 5
    Allocation Disagreement, 3
    
    >>> nt2.close()
    >>> os.remove( ntfn )
    """


if __name__ == "__main__":
    import doctest
    doctest.testmod()
